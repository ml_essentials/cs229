## **Lecture 5**
----
*so far we were learning about discriminative learning alg., today is about generative learning alg*

Generative learning alg. builds model to classify new features instead of searching for lines to seperate features.In test time it evaluates new input against every class model and looks for one which it fits best.

![[Pasted image 20220209135121.png]]

DLA:
Learn $p(y|x)$
(or learn $h_\theta(x) = \epsilon \{0 or 1\}$) *learns direct mapping to labels (kind like perceptron)*

GLA:
Learn $p(y|x)$ where x is feature and y is class

$p(y)$ is prior class. It means, like when the patient is walking into your office and you are trying to predict what type (b,m) of cancer he will have wo. any features.


#### Bayes rule
$p(y=1 | x ) = \frac{p(x|y=1)p(y=1)}{p(x)}$
where $p(x) = p(x|y=1) p(y=1) + p(x|y=0) p(y=0)$

We can notice that both  $p(x|y)$ and $p(x)$ are terms which are being learnt by GLA.

#### Gaussian Driscimant Analysis (GDA)
Suppose $x \epsilon \mathscr{R}^n$ *assuming features `x` are continuous values* (there is no $x_o = 1$ *bias*)

Assume $p(x|y)$ is Gaussian Distribution

Multivariate Gaussian:
$z \sim \mathscr{N}(\vec{\mu}, \Sigma)$
where $z \epsilon \mathscr{R}^n$
$\vec{\mu} \epsilon \mathscr{R}^n$
$\Sigma \epsilon \mathscr{R}^{nxn}$ *is 2 dimensional*

E[z] = $\mu$
E[z] is the same what Ez
Cov[z] = E[(z-$\mu$)$(z-\mu)^T$] = Ez$z^T$ - (Ez)$(Ez)^T$

Probability density function for Gaussian:
p(z) = $\frac{1}{(2\pi)^{\frac{1}{2}} |\Sigma|^{\frac{1}{\Sigma}}} exp(-\frac{1}{2} (x-\mu)^T \Sigma^{-1}(x-\mu))$

*$\mu$ and $\Sigma$ controls mean and variance of this density*

![[Pasted image 20220209143723.png]]
![[Pasted image 20220209144228.png]]
*this should be perfectly round circles*
![[Pasted image 20220209143828.png]]
![[Pasted image 20220209143928.png]]
![[Pasted image 20220209144054.png]]
![[Pasted image 20220209144302.png]]
*area under the curve is always 1 (integrates to one)*

*every covariance matrix $(\Sigma)$is symmetric*
*standard Gaussian mean is 0*

![[Pasted image 20220210102415.png]]

GDA model:
$p(x|y=0) = \frac{1}{(2\pi)^{\frac{1}{2}} |\Sigma|^{\frac{1}{\Sigma}}} exp(-\frac{1}{2} (x-\mu_0)^T \Sigma^{-1}(x-\mu_0))$

$p(x|y=1) = \frac{1}{(2\pi)^{\frac{1}{2}} |\Sigma|^{\frac{1}{\Sigma}}} exp(-\frac{1}{2} (x-\mu_1)^T \Sigma^{-1}(x-\mu_1))$

parameters of GDA model : $\mu_0, \mu_1. \Sigma, \phi$ *we use the same $\Sigma$ for both classes*

*the same covariant matrix but diff. means*

$p(y) = \phi^y(1-\phi)^{1-y}$   *$\phi$ is raw number*

-----

How to fit parameters 

Given training set $\{(x^{(i)}, y^{(i)})\}_{i=1}^m$
We are going to max. joint likelihood
$\mathscr{L}(\phi, \mu_0, \mu_1, \Sigma) = \prod_{i=1}^m p(x^{(i)}, y^{(i)} ; \phi, \mu_0, \mu_1, \Sigma) = \prod_{i=1}^m p(x^{(i)}| y^{(i)}) p(y^{(i)})$

Loss function for discriminative: *conditional likelihood*
$\mathscr{L}(\Theta) = \prod_{i=1}^m p(y^{(i)} | x^{(i)}, \Theta)$

Maximum likelihood estimation
max $\phi, \mu_0, \mu_1, \Sigma$
likelihood estimation: $\mathscr{l}(\phi, \mu_0, \mu_1, \Sigma) = log \mathscr{L}(...)$

--after math---

$\phi = \frac{\sum_{i=1}^m y^{(i)}}{m} = \frac{\sum_{i=1}^m \mathbb{1}(y^{(i)} = 1)}{m}$
$\mathbb{1}$ is indicator function which return 1 when statement inside is true or 0 when is false

$\mu_0 = \frac{\sum_{i=1}^m \mathbb{1}(y^{(i)} = 0) x^{(i)}}{\sum_{i=1}^m \mathbb{1}(y^{(i)} =0 )}$
*numerator is sum of feature vectors for examples with y=0

$\mu_1 = \frac{\sum_{i=1}^m \mathbb{1}(y^{(i)} = 1) x^{(i)}}{\sum_{i=1}^m \mathbb{1}(y^{(i)} = 1) }$
*numerator is sum of feature vectors for examples with y=1*

$\Sigma = \frac{1}{m} \sum_{i=1}^m (x^{(i)} - \mu_{y^{(i)}})(x^{(i)} - \mu_{y^{(i)}})^T$
*tries to find contours*

Prediction:
$arg max_y p(y|x) = arg max_y \frac{p(x|y)p(y)}{p(x)}$
$arg max$ is the value you need to plug in to obtain max value of equation

sometimes to save computation you just look for
$argmax(p(x|y)(p(y)))$

![[Pasted image 20220210123732.png]]

*using seperate $\mu$ and the same $\Sigma$ leads to decision boundary that is linear. Using 2 diff. $\Sigma$ is ok, but the output is not linear and you have more parameters to train*

---
Comparison. to logistic regression

for fixed $\phi, \mu_0, \mu_1, \Sigma$ lets plot  p(y=1|x; $\phi, \mu_0, \mu_1, \Sigma$) as function of x.

$p(y=1|x; \phi, \mu_0, \mu_1, \Sigma) = \frac{p(x|y =1; \mu_0, \mu_1, \Sigma)p(y=1; \phi)}{p(x;\phi, \mu_0, \mu_1, \Sigma)}$
$p(y=1; \phi) = \phi$ *it is just bernouli distr.*

![[Pasted image 20220210125028.png]]
![[Pasted image 20220210125102.png]]

![[Pasted image 20220210125822.png]]
$p(y=1) = 0.5$ because data split is 50/50 
For lower chart $0.5$ is a threshold because example taken in the middle can be classified for any particular class.
GDA assumptions implies that $p(y=1|x)$ is governed by a logistic function
Implication in opposite direction is not true.
GDA makes stronger set of assumptions, and LR makes weaker set of assumptions, because you can prove LR assumptions with GDA assumptions.
If you make strongly modeling assumption and they are correct the model will do better because you are tellling more informations to the model.
So if datasets meets the assumptions of GDA, it is probably better to use it than LR, but the problem is when the whole DS is not meeting these assumptions.

![[Pasted image 20220210130431.png]]
This statement is true for all GLM where these two distributions ($\lambda_0 \lambda_1$) varies only according to the natural parameter of exp. fam. distr.

*using LR doesn't require to know the distribution of data*
*with more data you can overcome telling the alg. more about the world => choosing LR over GDA*

Alg. has two sources of knowledge - dataset and assumption you tell it.
*GDA is more computing efficience*

----
#### Naive Bayes
Generative learning alg.
*we are trying to filter spam messages*
How to represent a feature vector x?
![[Pasted image 20220210133313.png]]

We got list of words as dictionary, and we take our text and check which word is present in it. When it is present we tick it as `1`, when not we tick it as `0` (binary representation).

$x \epsilon \{0,1\}^n$ *in our example $n=10000$ because we are using most popular 10k words*
in other way
$x_1 = \mathbb{1} \{$ word i appear in email $\}$

We want to model $p(x|y), p(y)$
possible values of x $2^n$ where n = 10k so you need $2^{10000}$ parameters *which is a lot*

Assume all $x_i$ are conditionally independent given y.
$p(x,......., X_{10000}|y) = p(x_1|y) p(x_2|x_1,y) p(x_3|x_1,x_2,y)  ... p(x_{10000}|...)$

it leads to assumption:
$p(x_1|y) p(x_2|y) p(x_3|y) ... p(x_{10000}|y) = \prod_{i=1}^n p(x_i|y)$
*it means that appearing one word is not connected with appearing another word, which can not be 100% true and we can get away with such assumption*

![[Pasted image 20220210135005.png]]

![[Pasted image 20220210135307.png]]
$\mathscr{L}(\phi_y, \phi_{j|y})$ leads to $\phi_y$ and $\phi_{j|y}$ after some nafty math

*this alg. is more efficient because estimating parameters is not iterative, it just counting*

*$x_j$ means feature and $x_i$ means training example*

## **Lecture 6**
----
In naive bayes
![[Pasted image 20220210152500.png]]
In the right there is $\phi_{6017|y=1}$ and $\phi_{6017|y=0}$

*NIPS is one of the most prestgious ML conferences*

Statistically is a bad idea to estimate something as 0 just because we haven't seen it occuring.

Here comes in help Laplace smoothing. It is a technique that helps address this problem.
Given dataset:
![[Pasted image 20220210152932.png]]

We are trying to predict next win of Standford footbal team.
![[Pasted image 20220210153040.png]]

Without Laplace smoothing we get 0 chance of winning next game.
With Laplace smoothing, which says `Add 1 to general number of losses and wins you are aware of`, we get more reasonable outcome.
*this works under assumption that dataset is Bayesian, which means, like the chance of the sun rising tomorrow is uniformly distributed,in the unit interval, anywhere from 0 to 1 and its set of observations is Bayesian - you don't need to worry about that assumption in practice*

![[Pasted image 20220210153738.png]]

![[Pasted image 20220210154036.png]]
*this is Naive Bayes MLE with Laplace smoothing*

![[Pasted image 20220210154551.png]]
The set of a problem is `Is house going to be sold in the next 30 days?`
We are discretizing value of House size (`x`) into 4 buckets. *typical rule of thumb is to discretize variables into 10 values/buckets*
$p(x_j|y)$ is now multinomial probability, where $x$ takes on of four values. It leads to that instead of Bernoulli distribution over two possible outcomes it can be probability mass function over four (in this particular scenario) possible outcomes

`Multivariate Bernoulli event model`
![[Pasted image 20220210155225.png]]
This is standard Naive Bayes feature representation. Here we throw away fact that the word `drugs` has appeared in our spam message twice, because `x` is represented by either `1` or `0`.
Also here `x` vector is always 10k length (amount of words in our dict)

`Multinomial event model`
Text specific representation:
![[Pasted image 20220210155515.png]]
Here length of feature vector depends on lenght of email message we received.

We are gonna build generative model with usage of `Multinomial event model`
![[Pasted image 20220210160638.png]]
On the first sight it looks the same as Naive Bayes equation but now the definition of $n$ is different - instead of fixed 10k it is length of email message and 
$p(x_j|y)$ instead of Bernoulli or binary probability it is now multinomial probability

We assume that chance of every word being `k` are independet

*`k` is one of possible word from our dictionary*

![[Pasted image 20220210161011.png]]
*the chance for any word being `k` word in non-spam email message*

with Laplace smoothing
![[Pasted image 20220210161355.png]]
*10k is our dictionary size*

*when there are words not included in your dict you can either throw them away or map to UWT - `unknown word token`*

Naive bayes is not very competetive with other learning alg. - for most problem LR will work better
The advantage is that is very computationally efficient and quick to implement.
*reasonable choice for implement smth quick and dirty*

*if you are starting out on a new application problem it is hard to know what's the hardest part of the problem*

---

#### Support Vector Machine

Let's set up `classification problem` with `non-linear decision boundary`.
Support Vector Machine is an alg. that helps us find potentially very very non-linear decision boundaries

![[Pasted image 20220210173109.png]]
One way of using LR here is to map our features ($x_1, x_2$) to high dimensional feature vector $\phi(x)$
If you do this and apply to that new augmented feature vector LR is able to learn non-linear decision boundaries.
*we don't know what set of features can lead us to proper elipse of decision boundary*

One of the reason behind using SVM is that it is turn-key alg.
They are not as effective as neural networks for many problems but are fast to use.

![[Pasted image 20220210173920.png]]
Optimal Margin Classifier is the basic building block for SVM
Kernel feature allows us to map our feature vector space to much higher dimensionnal set of features - it relieves us from a lot of burdden with manually picking features (like should be $x^2$ or $\sqrt{x}$ )
	
Let's develop optimal margin classifier

![[Pasted image 20220210174600.png]]
`functional margin` is how confidently and accurately do you classify an example
`>>` means much greater
`<<` means much less
It is very easy to cheat on functional margin - one of the method is to scale the parameters by scalar and it doesn't change decision boundary.

![[Pasted image 20220210174948.png]]
green line is better than red line because it has much bigger seperation.
`geometric margin` is term for that seperation.

![[Pasted image 20220210175211.png]]

![[Pasted image 20220210175515.png]]
`Previously` means in LR

![[Pasted image 20220210180153.png]]
Definition of function margins with respect to a single training example -  *how are you doing with this one particular example*
*hyperplane just means straight line but in high dimension*

![[Pasted image 20220210180623.png]]
Definition the function margin with respect to the entire set - *how are you doing on the worst example in your training*
*in this lecture we are assuming that dataset is linearly seperable*

Normalizing length of your parameters
![[Pasted image 20220210180951.png]]
$||w|| = 1$ - the value of it is our decision
such a method doesn't change the classification, it just scales parameters

![[Pasted image 20220210181426.png]]
`Geometric margin`, which we are gonna to define, is that distance between training example and decision line boundary. *it is euclidean distance de facto*

![[Pasted image 20220210181710.png]]

![[Pasted image 20220210181951.png]]
*look through all training examples geo. margins and pick the worst one*

![[Pasted image 20220210182405.png]]
*this problem says maximize geometric margin*
*the smaller $||w||$ is the less of a normalization division effect you have *
`S.t` means subject to that
In this form is not a convex optimization problem so it's difficult to solve wo a gradient descent and initially known local optima and so on. But it can be deformulated into the equivalent problem (with nafty math), which is a convex optimization problem:

![[Pasted image 20220210182536.png]]

Thanks to available software packages it's pretty easy to solve that equation. Thanks to that, with proper dataset, you can have baby SVM (wo kernels).

## **Lecture 7**
----
*SVM is basically optimal margin classifier + kernels*

![[Pasted image 20220211091652.png]]
![[Pasted image 20220211091839.png]]
Finding solution for this optimization problem lead to finding proper values for W and b which defines straight line (in our scenario) that defines a decision boundary.

![[Pasted image 20220211093030.png]]
When we choose $||w||$ to be equal to $\frac{1}{\gamma}$ our optimization objective becomes one above.

![[Pasted image 20220211093847.png]]
$\alpha_i x^{(i)}$ means `linear combination of training examples`.
*representer theorem says that you can make this assumption without any loss to model performance*

![[Pasted image 20220211094243.png]]
![[Pasted image 20220211094312.png]]
*we are comparing to LR*
*here $\alpha$ is lr*
If you run SGD or BGD LR, after every iteration, the parameters $\Theta$ are always a linear combination of the training examples 

![[Pasted image 20220211100052.png]]
*vector $w$ is always $90\degree$ to the decision boundary*

![[Pasted image 20220211095943.png]]
*vector $w$ lives in the span of training examples*

![[Pasted image 20220211100250.png]]
If we got training examples where $x_3 = 0$ it means that weight vector always $w_3 = 0$

![[Pasted image 20220211101014.png]]
Our new optimization objective. 
*$<x,z> = x^T z$ is the inner product*
The only place that feature vectors appears is in inner products.
If you can compute efficiently the inner products you can manipulate even inifinite dimensional feature vectors.

![[Pasted image 20220211101639.png]]
![[Pasted image 20220211101922.png]]
The entire alg. both the optimization objective you need to deal with during training as well as how you make predictions is expressed only in terms of inner products.

---
![[Pasted image 20220211102735.png]]
$<x^{(i)}, x^{(j)}>$ is going to mean the same what $<x,z>$
*2D or 100000 are just example values*
$K(x,z)$ is a kernel function.
Because whole alg. is basing on inner products and you can replace them with kernels which can be efficiently computed.

#1 example of kernel:
![[Pasted image 20220211104921.png]]
We map of feature vector $x$ from $n$ to $n^2$
![[Pasted image 20220211104957.png]]

![[Pasted image 20220211105414.png]]
Last formula is marching through all pairs of features and multiplying each other and adding to the sum.
We managed to turn $O(n^2)$ time calculation to $O(n)$

#2 example of kernel
![[Pasted image 20220211105913.png]]
*$c$ is some fixed real number*
Role of the constant $c$ - it trades off the relative weighting between binomial terms ($x_i, x_j$) compared to the first degree terms ($x_1, x_2, x_3$).
![[Pasted image 20220211110045.png]]
The required change in mapping our $\phi$ *the same is both for $\phi(x)$ and $\phi(z)$*

#3 example of kernel
![[Pasted image 20220211110419.png]]
*there is wrttien `features of monomial`*
the computation doesn't grow expontentially

Optimal Margin Classifier + kernel trick = SVM
*thanks to kernel trick your computional time scales only linearly*

----
Why is this a good idea?
https://www.youtube.com/watch?v=OdlNM96sHio&ab_channel=udiprod
![[Pasted image 20220211111823.png]]
You can use SVM to find very non linear decision boundary in starting dimension which can be a linear decision boundary in some high dimensional space.

![[Pasted image 20220211112107.png]]
think about $K$ as a similarity measure function

Question is: is this ok to use it as a kernel function?
![[Pasted image 20220211112737.png]]
*there is written `does there exist`*
*$K$ above the line represents the kernel function and below represents the matrix (kernel matrix or gram matrix) and later kernel function and then $K_{ij}$ is a kernel matrix*

![[Pasted image 20220211113058.png]]
![[Pasted image 20220211113211.png]]
It proves that kernel matrix is positive semi-definite
It is some kind of test whether K is a valid kernel function

Mercer's Theorem
![[Pasted image 20220211113500.png]]

![[Pasted image 20220211113536.png]]
![[Pasted image 20220211113829.png]]
*one of the most widely used kernels*
Gaussian kernel corresponds to use all monomial features.
Linear kernel is ommiting feature of mapping our vector X on the higher dimension vector.

*kernel trick is more general than SVM*
If you have any learning alg. that you can write in terms of inner products ($<x^{(i)}, x^{(i)}>$, you can apply kernel trick to it.
Linear regression, logistic regression, GLM, Perceptron can actually apply the kernel trick.
The single place this is most powerfully applied is SVM.
*in practice kernel trick is mostly used for SVM, very seldom in other alg.*

---
You don't want to find perfect but complicated decision boundary.
Sometimes, even in high dimensioanl feature space, you data may not be linearly separable.
*you don't need your alg. to have 0 errors on your training set*

$L_1$ norm soft margin SVM
![[Pasted image 20220211121321.png]]
*for the product of two things greater than zero, both of them need to have the same sign*
*as long as that product is greater than zero, it means that classify that product correctly*
SVM is asking not only to classify correctly but also with function margin of at least 1. And if you allow $\xi_i$ to be positive then that's relaxing that contraint.
You don't wan't $\xi_i$ to be too big so that's why you add to an optimization cost function a cost for making $\xi_i$ too big.
*the parameter $C$ is something you got to choose. It influences how much we want to get training examples right versus saying it's ok if you label a few terms out of this one*

![[Pasted image 20220211121617.png]]
`1` or `2` are values of functional margin for each example
Thanks to $\xi_i$  we can let that `red X` have lower function margin than 1.

![[Pasted image 20220211121946.png]]
Optimizing by worst case scenario allows one training example to have a huge impact on the decision boundary.$L_1$ norm allows to keep decision boundary and ignore that 1 outliner.

--- after some nafty math we can represent optim. problem like that ----

![[Pasted image 20220211122308.png]]
*there are very good software packages that are solving there constraint for you*

*it turns out that SVM with polynomial kernel works very well*
![[Pasted image 20220211122801.png]]

![[Pasted image 20220211122948.png]]
The goal is to make a prediction what is the function of this protein

![[Pasted image 20220211123451.png]]
*Knuth-Morris-Pratt alg could be used here - it is pretty decent to input a sequence of data and binary classify it*
There is many different kernel functions for many different unique types of input you might want to classify.

## **Lecture 8**
----
Tips how to apply learning algh. in practice.
*Bias/Variance is easy to understand, but hard to master*

*error decomposition, uniform convergence, vc dimension - concepts form learning theory*

![[Pasted image 20220211150509.png]]
![[Pasted image 20220211150430.png]]
Underfit - it does not capture the trend in data
*we want to avoid high bias*
HIgh bias means this learning alg. has very strong preconceptions that the data could be fit by linear functions

Overfitting - 
*we want to avoid high variance*
High variance - averaging over different random draws od the data, f.e. you friend collected slightly different dataset (house price to size of house) than your model has been trained and the decision boundary is totally different for this ds, than one the model has trained.

*standard work flow - usually you train an alg. and try to understand if the alg. has a problem of high bias or high variance*

![[Pasted image 20220211150835.png]]
Red line is overfiting (this model may have perfect score on training dataset) *this is much too complicated hypothesis*
Blue line in underfitting
Green line is just right


Regularization is one of the most effective way to avoid overfitting.
*technique is being often used*

![[Pasted image 20220211151501.png]]
*the example is Linear Regression* 
there is written `regularization`

![[Pasted image 20220211151712.png]]
if $\lambda$ is too large it can lead to underfitting

![[Pasted image 20220211152230.png]]
*this time is logistic regression, but it can be applied to any GLM*
Reason why SVM doesn't overfit all the time is because of forcing parameters to be small with usage of 
![[Pasted image 20220211152116.png]]
which works quite a like above regularization

![[Pasted image 20220211152655.png]]
*logistic regresion with regularization wll usually outperform naive bayes in classification, but wo regula. LR will badly overfit this data*
Rule of thumb: if you are not using regularization in LR it's nice if the number of examples is at least on the order of the number of a parameters you want to fit.

*we don't regul. per parameter because you end up with the $\lambda$ vector with the same amount of parameters like $\Theta$ you want to regul.*

Usually to make sure $\lambda$ affects parameters in the same way we normalize them to be on the same scale (substract out the mean and divide by the standard deviation). It will make also gradient descent run faster.

*high bias and high variance can happen in the same time*

![[Pasted image 20220212095001.png]]
`S` is a dataset
Here is shown case for Naive Bayes

![[Pasted image 20220212095330.png]]
If you assume that distribution is Gaussian and you take that $P(\Theta)$(Gaussian prior distribution) and do math it turns out you will end with the same regularization method as was described in the beginning.

![[Pasted image 20220212095942.png]]
there is written `maxium a posteriori estimation`

![[Pasted image 20220212101643.png]]
*higher degree of polynomial leads to lesser error on training set if you don't regularize*
*regularization with $\lambda$ can also lead to overfitting or underfitting*
on the right there is written `training error`

![[Pasted image 20220212102023.png]]
*It is good hygiene to split dataset into train/dev/test sets*
Given 10k dataset examples `S` we are trying to choose proper value for f.e. C or $\tau$

![[Pasted image 20220212102744.png]]
We shouldn't evaluate models on training set because you will end up using the highest possible polynomial because it is usually the most fitted for training set.
$S_{dev}$ and $S_{test}$ are datasets which alg. doesn't see during the training.

*when the test set is really really large the amount of overfitting is rather smaller*
*sometimes when you have a little amount of data you don't need to use $S_{test}$ to check the real error number, but you have to be aware that error of $S_{dev}$ you are going to report is biased*

How much data should go into each of set?
![[Pasted image 20220212104207.png]]
Rule of thumb shown here (60% > 20% > 20%) is for rather smaller datasets (several thousands examples). Generally you should choose enough data to make meaningful comparisons between different alg and if you expect to be better by a little fraction like 0.01% you need a lot of data to distinguish that.

![[Pasted image 20220212104313.png]]

*it is ok to evaluate your alg repeatedly on the same test set, but it's not okay to use this informations to make a decision about your learning alg.*

![[Pasted image 20220212105511.png]]
*is there a way to choose the polynomial of your model wo wasting data for $S_{dev}$?*
Last optional step is to refit the model with the best performance with 100% data.
*it is computationaly very expensive*
*k = 10 is the most common choose*
*you are holding only $\frac{1}{k}$ of the data*

*we rather think that all k estimates are somehow correlated*

![[Pasted image 20220212110224.png]]

*transfer learning, heterogeneity of input features - methods to boost performance where you have little dataset*

![[Pasted image 20220212111907.png]]
![[Pasted image 20220212112052.png]]
$\phi$ here is empty set of features.
It is called forward search because we are starting with empty set and try to add features to it. Backward search is when you start with all features and try to remove feature in every iteration.

## **Lecture 9**
----

*Ideas to be aware how machine learning works under the cover*

![[Pasted image 20220214091831.png]]
Main assumption is that there is a data distribution and examples of both train and test dataset come from that distribution.

![[Pasted image 20220214092246.png]]
Not random means there is no probability distribution associated with it.
This process is implying that there exists some true parameter (unknown constant) $\Theta^{*}$ or $h^{*}$ we wish to obtain from learning alg.

![[Pasted image 20220214095208.png]]
![[Pasted image 20220214095224.png]]
First is `Data view`, second `Parameter view`
Each point corresponds to the size of samples `m`.
Number of points is the number of epochs.

`Bias` means is checking is the sampling distribution kind of centered around the true parameter
`Variance` is measuring basically how dispersed the sampling dist. is

![[Pasted image 20220214100113.png]]
As we increase the size of the data $S$ the variance of $\hat{\Theta}$ will become smaller.
If you alg. has high bias it means no matter how big data you provide it keeps away from $\Theta^{*}$
There is no corelation between bias and variance.
Bias and variance are properties of alghorithm of given data size $m$.
HIgh variance means alg. can be easily influenced by noise data.

Fighting variance
![[Pasted image 20220214100524.png]]
![[Pasted image 20220214102040.png]]
![[Pasted image 20220214101202.png]]
![[Pasted image 20220214102021.png]]
$H$ is a class of hypothesis for given learning alg.
Even the best hypothesis $g$ can still make errors.
Bias is trying capture why $\hat{h}$ is staying away from $g$.

Fighting bias
![[Pasted image 20220214103321.png]]
By adding the regularization you are effectively shrinking the surface of $H$ class,by penalizing those hypotheses whose $\Theta$ is very large. It can lead to lowering the variance but adding extra bias.

*all this techniques can be applied to any learning alg.*

----

![[Pasted image 20220214110035.png]]
ERM - Empirical risk minimalization
*we are restricting ourselves to ERM learning alg. Thanks to that we can come up with more theoretical results*
ERM = reducing training loss

Uniform Convergence
![[Pasted image 20220214110101.png]]
Two main questions we want to answer are listed above *how one error influences another one*
`A` is event
`Need not be independent`

![[Pasted image 20220214110151.png]]
This Hoeffding's Inequality is limited to the Bernoulli distribution
`Pr` is probability
*the probability that the difference between estimated value and true value  becomes greater than some margin $\gamma$ is bouned by expression $2\exp(-2\gamma^2m)$*

![[Pasted image 20220214111801.png]]
*it is just one thick curve*

![[Pasted image 20220214111813.png]]
*if you apply Hoeffding's Ineq  to $h_i$ you get this*
*the probability that the gap between those two errors becomes greater than some margin $\gamma$ is bounded by the expression  $2\exp(-2\gamma^2m)$*
We want to extend this result to all $H$ (it is more practical that way).
The expresion gonna look different for two cases: 1. finite hypothesis class. 2. Infinite hypothesis class.

Finite hypothesis class $H$
![[Pasted image 20220214112444.png]]
*it is not very usefull, treat it rather as building block*
*it is Hoeffsing's Inequality + Union Bound + negative two sides*
![[Pasted image 20220214112724.png]]
*you can choose any two and resolve equations for the third*

![[Pasted image 20220214113218.png]]
*with probability at least 1 - $\delta$  the margin of error between empirical risk and true generalization gonna be less than $\gamma$  as long as the training size is bigger than this expression*
*as you increase `m` your dotted lines are gonna be closer to thick line*

----
Now we are trying to answer the second question to find relation between $\mathscr{E}(h^{*})$ and $\mathscr{E}(\hat{h})$
![[Pasted image 20220214113535.png]]

![[Pasted image 20220214114521.png]]
`w.p` is `with probability`

VC dimension *for infinity size of hypothesis class*
![[Pasted image 20220214114901.png]]
*we are trying to hook the size to the infinity size of $H$*
The sample complexity that you want is generally an order of the VC dimension to get good results.

## **Lecture 10**
----

Decision trees - non-linear model

![[Pasted image 20220217171604.png]]
![[Pasted image 20220217171624.png]]
`Lat()` is latitude
`Top-down` because we are starting with the overall region and we wanna slowly partition it up
`greedy` at each step we wanna pick the best partition possible
$j$ is feature number
$t$ is threshold

![[Pasted image 20220217172317.png]]
![[Pasted image 20220217173807.png]]
Sum of parent is already defined in this equation so, de facto, we are trying to minimize the sum of children losses.

![[Pasted image 20220217173127.png]]
The chart on the right seems to be better because it isolates more examples (500 vs 200) but from misclassification loss point of view they are the same.

![[Pasted image 20220217174116.png]]
That loss $L_{cross}$ means if someone knows the probability of examples to be a certain class, how much information you need to communicate to him exactly which class is this example (it is concept from Information Theory). In case when every example is one class you need to communicate far less info. than when there is even split.

![[Pasted image 20220217174817.png]]
Chart for cross entropy loss. Here point of parent loss is in the middle of line connecting $L(R_1)$ and $L(R_2)$ (midpoint) because the split of data is even. Generally that point can be on any place on the line. We can see here the gain of info.

![[Pasted image 20220217175415.png]]
Misclassification loss is not very sensitive, that's why we can't see info gain. If you end up with the points on the same side of the curve you don't see any sort of info gain.

![[Pasted image 20220217175502.png]]
Most functions that are successfuly used for decision splits look like the strictly concave function and gini is one of them.

---
![[Pasted image 20220217180159.png]]
![[Pasted image 20220217180214.png]]
$|R_m|$ is overal cardinality

![[Pasted image 20220217180607.png]]
![[Pasted image 20220217180307.png]]
Instead of having lattitude in degrees you can have three categories.
Generally you don't want to deal with too many categories because it can become quickly intractable to look through that many possible examples. Exception for that is binary classification.

You can end up, when running endlessly you decision tree model, with leaf for every data sample which is overfitting case. Decision trees are highly variance model.

Key takeaway is that we can use Decision trees for both regression and categorical tasks.

-----

#### Regularization
![[Pasted image 20220217181132.png]]
Regularization 4. is kind tricky to use because question with low loss change can lead to much better question in terms of loss decrease.

#### Runtime
![[Pasted image 20220217181859.png]]
![[Pasted image 20220217181925.png]]

#### Downsides of decistion trees
![[Pasted image 20220217182302.png]]
It doesn't have additive structure. It means you need to ask a lot of question to come up with straight line like on the chart and, even after that, you don't come near the wanted line. Decision trees have a lot of issues with these kind of structures where the features are interacting additively with one another.

#### Recap
![[Pasted image 20220217182626.png]]

#### Ensembling
![[Pasted image 20220217183509.png]]

We want to have as many different models that you are factoring as possible to increase $n$ which drives right term of equation $Var(X)$ down. And we also want to make sure that these models are as decorrelated as possible so $\rho$ goes down.

![[Pasted image 20220217184140.png]]
`Random Forests` is variant for `Bagging` for decision trees.

![[Pasted image 20220217184833.png]]
You are generating different bootstrap samples $Z$ from your training set with the replacement, because that way it makes hold the assumption that $S$ is our population.

![[Pasted image 20220217185142.png]]
You are training separate models with bootstrap samples and you are just aggregating them all together to get this bagging approach.

![[Pasted image 20220217190026.png]]
You can take as many bootstrap samples which increases $M$ and drives down the second term.
Increasing the number of bootstrap models you are training doesn't actually cause overfit anymore that it could be beforehand.
Subsampling is increasing bias, but generally decrease of the variance is bigger.

![[Pasted image 20220217190210.png]]

![[Pasted image 20220217190507.png]]
When you got one predictor that is very strong and gives you very good predictions your models will probably  always to use it causing all models to be very high correlated, but when you force to skip it it decorralets your models.

#### Boosting
![[Pasted image 20220217191523.png]]
By `Additive` it means that you are training one model and adding its predictions to the rest instead, like in Bagging, calculating the average of them.
Boosting is increasing the weights of faulty classified samples (ones in red box). Than we can recalculate decision boundary on the new data set. Next we can weight classifiers by ratio of properly classified samples.

Alg. on the right is called `Adaboost`