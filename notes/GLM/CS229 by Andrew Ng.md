**sources**
http://cs229.stanford.edu/syllabus-autumn2018.html
playlist: https://www.youtube.com/watch?v=jGwO_UgTS7I&list=PLoROMvodv4rMiGQp3WXShtMGgzqpfVfbU&ab_channel=stanfordonline
place for my code: https://gitlab.com/meldo8/raw_ai

----
## **Lecture 1**

- AI is revolution as was electricity

- https://research.google/teams/brain/

 - Convex Optimization - https://en.wikipedia.org/wiki/Convex_optimization

 - Arthur Samuel - author of checker program playing, which learnt by self-playing

 - Machine Learning - gives ability to computer to learn doing things, which was not explicitly programmed for

- Best practioners are very strategic in choosing their tools or algorithms

##### Supervised learning
The most common tool for ML
You are given datasets with `(x, y)`, and your goal is to find mapping from `x to y`

`y` is label

`x` as input can be both single value and multidimensional vector of features, even infinity-dimensionals vector

Regresion problem means value `y` which you are trying to predict is a `continous value`, f.e. given dataset with prices of houses and theirs square meters and the task is to check what will be the price of house you are going to sell


Classification problem means `y` takes on a discrete number of variables (there can be `n` classes), f.e. given dataset with malignant and benign tumors (`0` and `1` state) with new tumor as input you can try to forecast the possibility of it being malignant (`1`) basing on given dataset

 #### Unsupervised learning
 Given only `x`, without labels (`y`) the task is to find interesting structures in that data, f.e. market segmentation or social network analysis.
 
 Cocktail party problem
 
----
## **Lecture 2**

Job of learning alghorithm is to input a training set and output the hypothesis.

Key design choices for training algh.:
- What is a workflow?
- what is the dataset?
- what is the hypothesis?
- How this (output) represents the hypothesis?

#### How do you represent the hypothesis?
In linear regression it going to be an affinite function (technically):
 $$h(x) = \Theta_o + \Theta_1*x$$ 

More generally (with more complex `x` as input):
$$x_1 = size$$
$$x_2 = numbers of bedrooms$$
$$ h(x) = \Theta_o + \Theta_1*x_1 + \Theta_2*x_2$$

General format of thesis
$$ h(x) = \sum_{j=0}^{n} \Theta_j x_j $$
with $x_0 = 1$

Both $\Theta_j$ and $x_j$ can be matrixes.

$\Theta$ is called *parameters* and alg. job is to choose suitable parameters in order to be able make good predictions of houses prices (given example).

$m$ is number of training examples/ number of rows in table

$x$ is *inputs* / features

$y$ is *output* or target variable

$(x,y)$ is training example

$(x^{\text{(i)}}, y^{\text{(i)}})$ is i-th training example, not exponentation

So given
![[Pasted image 20220203155639.png]]

$x_1^{\text{(1)}} = 2104$
$x_1^{\text{(2)}} = 1416$
$x_2^{\text{(1)}} = 3$
$x_2^{\text{(2)}} = 2$

$n$ is number of features (in example $n=2$ size and bedrooms)

$\vec{0}$ is vector with only zeros

$tr(A)$ means  *trace of A* which is a sum of diagonal entries where $A$ is a matrix

#### Choose $\Theta$ such that $h(x) \approx y$ for training dataset

In order to do that we want to minimize cost function $J(\Theta) = \frac{1}{2} \sum_{i=1}^{m} (h(x^{\text{(i)}}) - y)^2$ *square difference*

*we add $\frac{1}{2}$ as convetion to help us later during differentiation. The formula stays the same - the effect for min. whole equatation will be the same for  the half of it*

#### Batch Gradient Descent implementation 

Start with random initial value of $\Theta$ (f.e. $\Theta = \vec{0}$).
Keep chaning $\Theta$ to reduce $J(\Theta)$

![[Pasted image 20220204073424.png]]

*We are reducing $J(\Theta)$ by taking little steps from current location until we find local minimum (or epochs end).*

$\Theta_j := \Theta_j - \alpha \frac{\partial}{\partial \Theta_j} J(\Theta)$
where $\alpha$ is learning rate

*Differentiation allows to identify the steepest direction of the function*
*We are subtracting values fro $\Theta$ rather than adding, because we want to go down the quadratic function*

From calculus we can write:
$\Theta_j := \Theta_j - \alpha \sum_{i=1}^{m}(h_\Theta(x^{\text{(i)}}) - y^{\text{(i)}}) x_j^{\text{(i)}}$
*Repeate until convergence for $j = (0, 1, .... n)$*

Iteration of this alg. could look like this:
![[Pasted image 20220204080853.png]]

*Too large $\alpha$ can overshoot local minima and too little can take a lot of epochs to complete. Try few values and see which is most effective. When cost function is increasing its sign that learning rate is too high.*

Our starting hypothesis could be like that: 

![[Pasted image 20220204080351.png]]

and by iterations and updating the weights we want to achieve such a hypothesis:

![[Pasted image 20220204080435.png]]

Described alg. is called *Batch gradient descent* because we are calculating loss over the whole dataset (symbol $\sum_{i=1}^{m}$). Its disadvantage is to order make one update you need to calculate the entire database, which can be very large and the whole process will be very slow.

#### Stochastic Gradient Descent impl.

Formula:
Repeat {
		for i=1..m {   
			$\Theta_j := \Theta_j - \alpha (h_\Theta(x^{\text{(i)}}) - y^{\text{(i)}}) x^{\text{(i)}}_j$
		}  
}
Now we are updating $\Theta_j$ with only one example at the time.

Probably path of SGD alg. to local minimum:
![[Pasted image 20220204082924.png]]
*Decreasing learning rate will couse size of oscilations to decrease*

SGB will never converge. Its path is way more noise that BGD

*If your dataset allows you to use BGD it's usually better to do it, because you got one thing less to worrry about, but usually the dataset is too large.*

#### Normal equation
Matrix derivation:

$\nabla_A f(A)$ = 
$$  
	\begin{bmatrix}
	\frac{\partial f}{\partial A_{0,0}} & ... & \frac{\partial f}{\partial A_{0, j}}\\  
	\frac{\partial f}{\partial A_{i, 0}} & ... & \frac{\partial f}{\partial A_{i,j}}
	\end{bmatrix}
$$

where $A \epsilon \mathbb{R}^{ixj}$

Based on formula upon:
To minimize function of cost we set it to 0
$\nabla_\Theta J(\Theta) = \vec{0}$

To derivate normal equation we take derivates with respect to Theta and set them to 0.
Than we try to resolve the equations and we got the value for Theta which is global minimum.

if $A$ is a square matrix ($A \epsilon \mathbb{R}^{n x n}$)
$tr(A) = tr(A^T)$

GIven $f(A) = tr(AB)$
$\nabla_A F(A) = B^T$

$tr(AB) = tr(BA)$
$tr(ABC) = tr(CAB)$
$\nabla_A tr(A A^T C) = CA + C^T A$

---

$J(\Theta) = \frac{1}{2} \sum_{i=1}^{m} (h(x^{(i)}) - y^{(i)})^2 = \frac{1}{2} (X\Theta - y)^T (X\Theta-y)$

where $X\Theta =$
$$  
	\begin{bmatrix}
	(x^1)^T \\
	(x^2)^T	\\
	... \\
	(x^m)^T \\
	\end{bmatrix}
$$
$$  
	\begin{bmatrix}
	\Theta_0 \\
	\Theta_1\\
	... \\
	\Theta_n \\
	\end{bmatrix}
$$
=
$$
\begin{bmatrix}
h_\Theta(x^{(1)}) \\
h_\Theta(x^{(2)})\\
... \\
h_\Theta(x^{(m)}) \\
\end{bmatrix}
$$

and $\vec(y) =$
$$  
	\begin{bmatrix}
	y^{(1)} \\
	y^{(2)} \\
	... \\
	y^{(m)}  \\
	\end{bmatrix}
$$

$Z^T Z = \sum Z^2$

Let's derive the normal equation:
$\nabla_\Theta  J(\Theta) = \nabla_\Theta \frac{1}{2} \sum_{i=1}^{m} (h(x^{(i)}) - y^{(i)})^2 = \nabla_\Theta \frac{1}{2} (X\Theta - y)^T (X\Theta-y) = X^TX\Theta - X^Ty$

$X^TX\Theta - X^Ty = 0$
$X^TX\Theta = X^Ty$ *normal equations*
$\Theta = (X^TX)^{-1} X^T y$ *optimum value of Theta*

with that equation we can get the optimum value of Theta in just one step

If $X$ is not-invertible usually means X has redundant features.

----
## **Lecture 3**
*we are still using example of datasets of houses and their prices*

Picking a way of showing the features (`X`) in hypothesis can lead to different lines plotted over our datasets. We need to pick right representation of features to fit our data most accurately, f.e. $\Theta_0 + \Theta_1 x_1$ will plot straight line, but $\Theta_0 + \Theta_1 x_1 + \Theta_2 x^2$ will plot a curved one.

#### Locally weighted regression
Alg. which helps us finding most proper representation of our features.

*parametric learning alg.* - fit some fixed set of parameters (like $\Theta_i$) to data, f.e. Linear Regression .

*non-parametric learning alg.*  - the amount of data/parameters you need to keep (f.e. in computer memory) grows (linearly) with the size of data, f.e. Locally weighted regression.
*you probably don't want to have some big dataset here. You don't need to fiddle manually with features. *

Given dataset
![[Pasted image 20220204202831.png]]


To evaluate $h$ at certain $x$:
LR: fit $\Theta$ to minimize $J(\Theta)$ and return $\Theta_x^T$

Locally weighted regression: fit $\Theta$ to minimize $\sum_{i=1}^{m} w^{(i)}(y^{(i)} - \Theta^Tx^{(i)})^2$ where $w^{(i)}$ is a weight function.
$w^{(i)} = exp(- \frac{(x^{(i)} - x)^2}{2\tau^2})$ *default choice*

Informally:
![[Pasted image 20220204202901.png]]

*we have input `x` and we take nearest neighbourhood of it in order to draw line (like the green one) and estimate value of our input*

if $|x^{(i)} - x|$ is small than $w^{(i)} \approx 1$
if $|x^{(i)} - x|$ is large than $w^{(i)} \approx 0$
*it means that all training features that are far away from our test input will be mult. by 0, and those close to a test input will be mult. by 1, thanks to that we will be able to estimate value of test input `x`*

$\tau$ - bandwith parameter, hyper parameter of alghorithm, it has effect on overfitting or underfitting

*potential scenario of usage: lots of data, small amount of features (like 2 or 3). Then you don't need to think which features to use*

#### Probabilistic interpretation of linear regression
#### Why least squares? (why square error?)

Assume $y^{(1)} = \Theta^Tx^{(i)} + \epsilon^{(i)}$ where $\epsilon$ is noise, unmodelled effects
$\epsilon^{(i)} \sim \mathcal{N}(0, \sigma^2)$ where 

$\mathcal{N}(0, \sigma^2)$ is normal distribution (Gaussian Distribution). It means that $P(\epsilon^{(i)}) = \frac{1}{\sqrt{2\pi} \sigma} exp(-\frac{(\epsilon^{(i)})^2}{2\tau^2})$, where $P$ is probability. It integrates to 1.
It creates such a shape - Gaussian bell-shaped curve with mean 0 and variance $\sigma^2$:
![[Pasted image 20220205070833.png]]
where $\sigma$ control width of the Gaussian.

IID stands for *Idependently and Identically Distributed* which means the same chaos factor is independent for every example from dataset.
*it is assumption from statistic to be able create better models, it might be not 100% true*

This implies:
$P(y^{(i)}|x^{(i)};\Theta) = \frac{1}{\sqrt{2\pi} \sigma} exp(-\frac{(y^{(i)}) - \Theta^T x^{(i)})^2}{2\tau^2})$

$y^{(i)}|x^{(i)};\Theta \sim \mathcal{N}(\Theta^T x^{(i)}, \sigma^2)$

*given x and theta what's probability of y*

$\mathscr{L}(\Theta) = P(\vec{y} | x;\Theta) = \prod_{i=1}^m P(y^{(i)} | x^{(i)};\Theta)$

*likelihood of parameters is the same as probability of the data. usage is just a convention*

$\mathscr{l}(\Theta) = log \mathscr{L}(\Theta) = ... =$
$= m log\frac{1}{\sqrt{2\pi}\sigma} + \sum_{i=1}^m(-\frac{(y^{(i)} -\Theta^Tx^{(i)})^2}{2\tau^2})$
*we are taking $log$ because its easier to maximize from Linear Algebra point of view. Log is a strictly monotonically increasing function which means that value of $\Theta$ that maximize $\mathscr{l}(\Theta)$should be the same that max. $\mathscr{L}(\Theta)$ *

MLE: maximum likelihood estimation *one of well tested letters in statistics*
Choose $\Theta$ to maximize $\mathscr{L}(\Theta)$ 

Trying to maximize $\mathscr{L}(\Theta)$  we are looking for value of $\Theta$ that has a highest likelihood.
$m log\frac{1}{\sqrt{2\pi}\sigma}$ is constant, so we are trying with the second term: $\sum_{i=1}^m(-\frac{(y^{(i)} -\Theta^Tx^{(i)})^2}{2\tau^2})$. Because of minus symbol $-$ and that $\sigma^2$ is another constant we are going to choose $\Theta$ to minimize $\frac{1}{2} \sum_{i=1}^m(y^{(i)} - \Theta^T x^{(i)})^2 = J(\Theta)$

*It proofs that choosing the value of $\Theta$ to minimize the least squares errors is just finding maximum likelihood estimate for the parameters $\Theta$ (with certain assumptins that was made -IID and error terms are Gaussian)*

*MLE <=> least square errors*
*in practice you rather don't bother about IID*

#### Apply alg.  to classification problem (logistic regression)
$y \epsilon \{0,1\}$ binary classification

*Linear regression is not a good alg. for classification because it can be very badly influenced by data f.e. *
![[Pasted image 20220205163847.png]]

Logistic regression
*probably most often used alg. for classification*

Want hypothesis $h_\Theta(x) \epsilon [0, 1]$  *outputs*
$h_\Theta(x) = g(\Theta^Tx)= \frac{1}{1+e^{-\Theta^Tx}}$
where $g(Z) = \frac{1}{1+e^{-Z}}$ is *sigmoid* or *logistic* function with shape:
![[Pasted image 20220205164418.png]]

*when deciding how hypothesis should look like you got to make a decision and choosing sigmoid function here is one of them*

Assumptions (with usage of example of breast cancer dataset from 1. lecture):
$P(y=1|x;\Theta) = h_\Theta(x)$ *given feature x what are the chance that this tumor is malignant*
$P(y=0|x;\Theta) = 1- h_\Theta(x)$

Taking these 2 equations we can compress them into:
$P(y|x;\Theta) = h(x)^y (1-h(x))^{1-y}$

$\mathscr{L}(\Theta) = P(\vec{y} | x;\Theta) = \prod_{i=1}^m P(y^{(i)} | x^{(i)};\Theta) =  \prod_{i=1}^m h_\Theta(x^{(i)})^{y^{(i)}} (1 - h_\Theta(x^{(i)})^{1-y^{(i)}}$

*max. log likelihood*
$\mathscr{l}(\Theta) = log \mathscr{L}(\Theta) = \sum_{i=1}^m y^{(i)} log h_\Theta(x^{(i)}) + (1-y^{(i)})log(1-h_\Theta(x^{(i)}))$
We need to choose such $\Theta$ to max. $\mathscr{l}(\Theta)$

We are going to use *Batch gradient descent*
$\Theta_j := \Theta_j + \alpha \frac{\partial}{\partial \Theta_j}  \mathscr{l}(\Theta)$

*first change compared to linear regression is in LG we got $J(\Theta)$ and here is $\mathscr{l}(\Theta)$*
*second is that in linear regresion we got $-$ because we were trying to min. loss function and here we got $+$ because we are trying to max likelihood*

---after some nafty math---

$\Theta_j := \Theta_j + \alpha \sum_{i=1}^m (y^{(i)} - h_\Theta(x^{(i)}))x_j^{(i)}$

*there is no chance of local max in case of this function (log-likelihood), because it is concave function - the only max is global maximum. It is another reason to choose it - it guarantess only one global max.*

For now what we were trying to do was:
trying to predict malignancy of tumor given training set with $(x^{(i)}, y^{(i)})$,
define likelihood and log-likelihood and then we need to have an alg. (f.e. gradient descent or ascent) to try find a value of $\Theta$ to max log-likelihood.
Having choosen the value of $\Theta$ we use $h(\Theta)$ to estimate chances of malignancy for new tumors.


#### Newton's method
*It allows much bigger jumps than gradient ascent.*
 
 When $\Theta$ is a real number
 Have  func $f$
 want to find $\Theta$ such that $f(\Theta) = 0$
 in order to do so we need to max. $\mathscr{l}(\Theta)$ which means $\mathscr{l}'(\Theta) = 0$
 
 ![[Pasted image 20220205174343.png]]
 
 $\Theta^{(1)} = \Theta^{(0)} - \Delta$
 $f'(\Theta)^{(0)}) = \frac{f(\Theta^{(0)})}{\Delta}$
 $\Delta = \frac{f(\Theta^{(0)})}{f'(\Theta^{(0)})}$
 
 which lead to:
 
$\Theta^{(t-1)} := \Theta^{(t)} - \frac{f(\Theta^{(t)})}{f'(\Theta^{(t)})}$

let $f(\Theta) = \mathscr{l}'(\Theta)$
which gives us
$\Theta^{(t-1)} := \Theta^{(t)} - \frac{\mathscr{l}'(\Theta^{(t)})}{\mathscr{l}''(\Theta^{(t)})}$

*quadratic convergance means f.e. 0.01 error after one iteration goes to 0.0001 than to 0.00000001 so it shrinks very fasy with every iter*

When $\Theta$ is a vector
$\Theta^{(t-1)} := \Theta^{(t)} - \mathscr{H}^{-1} \nabla_{\Theta} \mathscr{l}$
where $\mathscr{H}$ is Hessian matrix
*$\nabla_{\Theta} \mathscr{l} = \mathscr{l}'(\Theta^{(t)})$ is still gradient of function
$\mathscr{H}^{-1} = \mathscr{l}''(\Theta^{(t)})$ is Hessian of function*

*disadvantage of newton method is that when $\Theta$ is a vector in high dimensional problems each step is much more expensive*

*vector of 50 parameters is good to use newtons method but 10k is not*

----
## **Lecture 4**

#### Perceptron
*not widely used in practice, studied for hist. reasons*
Logistic regression uses sigmoid function which squeezes all from $-\infty$ to $\infty$ between 0 and 1.
$g(z) = \frac{1}{1+e^{(-z)}}$

Perceptron uses different function
$\begin{equation}
  g(z) =
    \begin{cases}
      1 & z \geq 0\\
      0 & z \textless 0
    \end{cases}       
\end{equation}$

of shape:
![[Pasted image 20220206082434.png]]

it leads to hypothesis function like:
$h_\Theta(x) = g(\Theta^Tx)$

Update rules: *on surface similiar to logistic regression*
$\Theta_j := \Theta_j + \alpha (y^{(i)} - h_\Theta(x^{(i)}))x_j^{(i)}$

0 -> when alg. got right prediction
-1 -> wrong pred. $y^{(i)} = 1$
1 -> wrong pred. $y^{(i)} = 0$

![[Pasted image 20220206083516.png]]

dotted black line is our decision boundary
$\Theta$ vector should be normal to our dotted line
When new feature `box` comes and its under our decision boundary, we recalculate that hypothesis so the blue dotted line includes new feature.

we want $\Theta$ to be 
$\Theta \approx x | y = 1$
$\Theta \not\approx x | y = 0$

The most naive way to do it is by adding vector component  to $\Theta$

#### Exponential families
Class of probability distributions, very closely related to GLM

$PDF$ is probability distribution function

PDF of exponential family can be written in the form:
$p(y;\eta) = b(y) exp(\eta^T T(y) - a(\eta)) = \frac{b(y) exp(\eta^TT(y))}{e^{a(\eta)}}$
$y$ is data
$\eta$ is natural parameter
$T(y)$ is sufficient statistic *will be equal to y for all distributions seen in this course*
$b(y)$ is base measure
$a(\eta)$ is log-partition *normalizing constant of the probability distribution*

Dimension of $\eta$ should match dimension of $T(\eta)$.
$y, b(y), a(\eta)$ are scalars

For any choice of $a(\eta), b(y), T(y)$ as long as expresion above integrates to one, you have a family in the expotential family.

##### Bernoulli distribution (for binary data)
$\phi$ - probability of the event happening
$p(y;\phi)= \phi^y (1-\phi)^{(1-y)}$ - PDF form of Bernoulli
$= exp(log(\phi^y (1-\phi)^{(1-y)})) = exp(log(\frac{\phi}{1-\phi})y + log(1-\phi))$

Matching this statement with the general one: *first one*
$b(y) = 1$
$T(y) = y$
$\eta = log(\frac{\phi}{1-\phi})=> \phi = \frac{1}{1+e^{-\eta}}$
$a(\eta) = log(1-\phi)) => -log(1 - \frac{1}{1+e^{-\eta}} = log(1+e^\eta)$

*it verifies that Bernoulli distribution is part of expotential family*

##### Gaussian (with fixed variance) (used for real value data)
It has two parameters - variance and mean.

Assume that variance $\sigma^2 = 1$
$p(y;\mu) = \frac{1}{\sqrt{2\pi}} exp(-\frac{(y-\mu)^2}{2})$
$= \frac{1}{\sqrt{2\pi}} e^{\frac{-y^2}{2}} exp(\mu y - \frac{1}{2}\mu^2)$

Matching this statement with the general one: *first one*
$b(y) = \frac{1}{\sqrt{2\pi}} e^{\frac{-y^2}{2}}$
$T(y) = y$
$\eta = \mu$
$a(\eta) = \frac{\mu^2}{2} = \frac{\eta^2}{2}$

*we use exponential families because it has nice math. properties*

1. Performing MLE on exponential family, which is parametrized with natural parameters, optimization problem is concave.

MLE with respect to $\eta$ => concave
NLL (negative likelihood - MLE with flip sing) => convex

2. $E[y;\eta] = \frac{\partial}{\partial \eta} a(\eta)$ *first derivative*

Differentiated $a(\eta)$ with respect to $\eta$ gives us another function with respect to $\eta$, which is the mean of the distribution as parametrized by $\eta$

3.  Variance[y;$\eta$] = $\frac{\partial^2}{\partial \eta^2} a(\eta)$ *second derivative*

*in case of mutli-variate gaussian $\eta$ would be a vector and instead of $\partial$ we would have Hessian matrix*

#### Generalized linear models
*for count value (non-negative integers) use Poisson distribution*
*for positive real value integers like volume of some object or a time to event where you only predict in the future use Gamma or Exponential (distribution)*
*probability distributions over probability distributions, then use Beta, Disschlet*


Assumptions for GLM / Design choices
1. $y|x;\Theta \sim Exponential family (\eta)$ *is member of*
2. $\eta = \Theta^Tx$ where $\Theta \epsilon \mathscr{R}^n$ , $x\epsilon \mathscr{R}^m$

3. Test time: output $E[y|x;\Theta]$ for a new $x$

$h_\Theta(x) = E[y|x;\Theta]$

![[Pasted image 20220206164556.png]]
*we are training $\Theta$ to predict parameters of exp. family distribution whose mean is our prediction - $y$ *
*during training time we are learning $\Theta$ not parameters of exp. family distri.*

##### GLM training

Learning update rule:
$\Theta_j := \Theta_j + \alpha(y^{(i)} - h_\Theta(x^{(i)}))x_j^{(i)}$

no matter of exp. fam. distr. choice or loss function

for batch gradient descent
$\Theta_j := \Theta_j + \alpha(\sum_{i=1}^m (y^{(i)} - h_\Theta(x^{(i)}))x_j^{(i)}$

Terminology:
$\mu = E[y;\eta] = g(\eta)$ is canonical response function
$\eta = g^{-1}(\mu)$ is canonical link function

![[Pasted image 20220206165904.png]]
three parametrizations:
model param - $\Theta$ *the only thing that is learned*
natural param - $\eta$
canocial param - $\phi$ | $\mu$ $\sigma^2$ | $\lambda$ for Poisson

In logistic regression:
$h_\Theta(x) = E[y|x;\Theta] = \phi$ because we are basing on Bernoulli Distr.
$= \frac{1}{1+e^{-\eta}} = \frac{1}{1+e^{-\Theta^Tx}}$

##### Assumptions visualizations
Regression
![[Pasted image 20220206171749.png]]
![[Pasted image 20220206171732.png]]

Classification
![[Pasted image 20220206171847.png]]
![[Pasted image 20220206171902.png]]

#### Softmax regression
*yet another member of GML family*
*it is also doing cross entropy minimalization*

Given dataset
![[Pasted image 20220206172319.png]]

$x^{(i)} \epsilon \mathscr{R}^m$
label $y = [\{0,1\}^k]$ *it is one-hot vector*
k is number of classes

$\Theta_{class} \epsilon \mathscr{R}^n$ *can be represented as matrix*

![[Pasted image 20220206172850.png]]

for a new input `x` near the `square` class probability distribution may look like this:
![[Pasted image 20220206173153.png]]
![[Pasted image 20220206173254.png]]

*normalize means divide everything by the sum of it*
![[Pasted image 20220206173442.png]]
It outputs the probability of all the classes

Training purpose is to change the output distribution (one above) to look like our ground truth distribution which could be like this:
![[Pasted image 20220206173719.png]]

General formula
$CrossEnt(p, \hat{p}) = \sum_{y \epsilon {k}} ( p(y) log \hat{p}(y) ) =  - log \hat{p}(y) = - log \frac{e^{\Theta_{class}^Tx}}{\sum_{c \epsilon k} e^{\Theta_{class}^Tx}}$

*output of it is treated as a loss and gradient descent is being performed on it*