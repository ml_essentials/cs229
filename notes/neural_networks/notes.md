## **Lecture 11**
----

![[Pasted image 20220217203748.png]]
Deep learning is a set of techniques. It is really computationally expensive.

![[Pasted image 20220217205757.png]]
Loss function is working as a proxy for your network function parameters (`weights`) you are trying to find.
Now the number of model parameters depends of our input.

![[Pasted image 20220217205815.png]]
Shipping a model means you have found a right architecture and right values of parameters for it.

![[Pasted image 20220217211931.png]]
$[1]$ in $a^{[1]}_1$ means layer and its index.The subset is number identifying the neuron inside the layer.
Neuron is going to evolve depending how you label your dataset.
We can train neurons independently, because they don't communicate with each other.
$Z$ is a linear part of selected neuron.

![[Pasted image 20220217213455.png]]
Now activation is softmax. The sum of the outputs of this networks have to sum up to 1.
$(1 1 0)$ input label won't work because sum has to be 1 and the network will probably match first and second probability to $\frac{1}{2}$ and third one to $0$.

![[Pasted image 20220217213624.png]]
Loss function from first goal is incorrect for second goal because it assumes that there are only two classes `1` and `0` so cat is present or absent.

![[Pasted image 20220217214132.png]]
Loss function for third case. In this case the derivate of loss function would be harder (if we would use the same as for second goal) because of the denominator which depends on all weights. That's why we need to use different loss function.

------
![[Pasted image 20220217220655.png]]
Layer denotes neurons that are not connected to each other.
Input layer is the first layer - it sees input of ground feature.
Output layer (third in this scenerio) is the layer that sees ultimate output which is compared later to the ground truth.
The reason we use term `hidden layer` is because the inputs and the outputs are hidden from this layer.
First layer are usually able to detect edges from our input image. The following hidden layers are usually able to build structures upon this edges like mouth or ears.

![[Pasted image 20220217221617.png]]
As a human you can make some preassumptions basing on your knowledge f.e. that zip code and wealth of neighbourhood can tell you about the school quality. For leting the neural network figure it out we use fully connected layers.
`Fully connected layer` means we are connecting every input to the first layer and its output to the second layer and so on in order to find representative connotations.

`black box model` because we are not able to understand links which are being found by nn for every feature.
`end-to-end learning` means we have an input and ground truth and we don't constrain the network in the middle.

![[Pasted image 20220217222427.png]]
there is $3$ in the beggining because we are connecting 3 neurons.

![[Pasted image 20220217223823.png]]
with red there is written `id of the example`
Because we are trying to add vector of shape $(3,m)$ to vector of shape $3,1$ and it is not possible in math we need to broadcast the second vector, which mean to repeat it $m$ times in order to fit the first vector.

First layers can only see edges because they see all the pixels which is very granular information to them. When they figure out where are what edges they pass that info further and the next  layers is able to build something more upon these statements.

----
![[Pasted image 20220217224803.png]]
We want to calculate the cost function that will be then derived to give us the direction of the gradients.
The complexity of derivation cost function J or loss function L is the same, because of the derivation linearity in case of J function we need to take only one step more - to sum all derivations of L.

![[Pasted image 20220217225806.png]]
We want to start with derivate of $W^3$ because its connection with loss function is easier to understand is that they are closer to each other.

## **Lecture 12**
----

We are using batch examples in order to parallelize the computation (vectorization) and use GPUs.

#### Backpropagation
![[Pasted image 20220218133815.png]]
![[Pasted image 20220218133837.png]]
We are counting $\frac{\partial Z}{\partial w^{[3]}}$ instead of $\frac{\partial J}{\partial w^{[3]}}$ because it is easier from math point of view and, due to linearity of this equations, sum of first equation is going to be our second equation.
$a^{[3]} = \sigma(w^{[3]} a^{[2]} + b^{[3]})$

![[Pasted image 20220218134547.png]]
Reasoning behind Transpose of a.
Taking sigmoid doesn't change the shape.
The output of this derivate should have the same size as the vector we are taking that derivate in respect to (in this example it is $w^{[3]}$ which size is $(1,2)$).
Because the output of this derivate is $a$ and it should, due to math rules, match the size of $w^{[3]}$ in this case, we must transpose our output.

We are continuing to count the derivate of loss for third layer.
![[Pasted image 20220218150130.png]]
![[Pasted image 20220218150153.png]]
In the second layer derivate we don't want to take the derivation in respect to $w^{[3]}$ or $b^{[3]}$ because we will get stuck and we want to backpropagate.

![[Pasted image 20220218153448.png]]
![[Pasted image 20220218153842.png]]
The shape of weight which input are 3 neurons and output is for 2 neurons will be $(2,3)$.
For computational efficiency we usually want to cache computer values in forward propagation in order to use them during backward propagation and save the time.
It costs us memory of course.

#### Improving your NNs
Using only derivations (as described above) won't lead to have a good network. You need to improve it using other tricks.

![[Pasted image 20220218155114.png]]
![[Pasted image 20220218155206.png]]
We can try different activation functions.
Sigmoid works very well in linear regime but has problems with working in saturating regimes. Big values of net output, after activation, are going to be close to 1 or 0. Because of that gradient is going to vanish (is going to be very small). That's why it may be hard to update weights that can have big values and that process can be very very slowly.
$tanh$ has the same property as sigmoid.
ReLU doesn't have that problem.

Why do we need activation functions?
![[Pasted image 20220218160217.png]]
![[Pasted image 20220218160238.png]]

If you don't choose the activation function, no matter how deep is your network, is going to be equivalent to a linear regression. It means that the complexity of the network comes from the activation function.

f.e. `ReLU layer` means it is a fully connected layer with ReLU activation.

----
![[Pasted image 20220218161345.png]]
![[Pasted image 20220218161412.png]]
If net output is too big or too low it will lead to saturated activations. To avoid that you can normalize your input in order to avoid too big values of W. Also with unnormalized input is harder to find minimum of loss function as we always wanted to take the steepest slope which can not lead directly to that minimum. When the input is normalized the steepest slope of gradient is always pointing to the minimum (which sometimes can cause fewer iterations to reach it).
$\mu$ and $\sigma$ are computed over the training set and they have to be used also on the test set.

![[Pasted image 20220218162510.png]]
![[Pasted image 20220218162526.png]]
Values that are bigger than 1 will cause exploding gradient and lesser than 1 will vanish it. You need to init your values with proper values.

Various weight init. schemas
![[Pasted image 20220218162935.png]]
For sigmoid is better with 1 in numerator is better for sigmoid and 2 is better for ReLU

-----

Batch gradient descent is cool because you can use vectorization.
Stochastic gradient descent updates are very quick.

![[Pasted image 20220218165234.png]]
![[Pasted image 20220218165542.png]]
![[Pasted image 20220218165500.png]]
The smaller the batch the more stochasticity so the more noise you will have on your graph.
Red iterations are going to be computed faster than the green one.

![[Pasted image 20220218165910.png]]
Green line is with momentum, red is without.
You can think momentum as a friction.

![[Pasted image 20220218170059.png]]
v is velocity, it tracks the direction that we should take regarding the current and past updates with a factor $\Beta$ that is going to be weights.

## **Lecture 13**
----

Implementation of learning alg. rarely works on the first time.

![[Pasted image 20220221074344.png]]
We are building an anti-spam classifier. And we started with implementing logistic regulatization.

![[Pasted image 20220221074952.png]]
Usually people will choose one of the approach basing on their previous experience or just randomly.
Doing team brainstorm and evaluating each option is very good approach.
The common diagnostic is `bias vs variance` diagnostic.

![[Pasted image 20220221075028.png]]
![[Pasted image 20220221075257.png]]
Average training error will increase as you increase your dataset, because it is harder for alg. to fit the data.

![[Pasted image 20220221075710.png]]
High bias may be cause f.e. by too simple learing alg., not enough features.

It is hard to know what can go wrong with your learning alg. That's why you should start with something quick and dirty (f.e. logistic regression) and check the bias-variance error. Basing on it decide what to do next.

![[Pasted image 20220221081131.png]]
![[Pasted image 20220221081411.png]]
![[Pasted image 20220221081646.png]]
![[Pasted image 20220221081950.png]]

We can't maximize $a(\Theta)$ directly because it is not differentiable and we have not a good alg. to do that.

![[Pasted image 20220221082327.png]]
We are taking the cost function $J$ and computing it with weights parameters from both SVM and BLR and compare the outputs.

![[Pasted image 20220221082912.png]]

![[Pasted image 20220221083349.png]]
`Try using a SVM` changes the optimization objective completely.

There is not an obious thing to do when you find that you are optimizing wrong objective.

![[Pasted image 20220221084002.png]]

Simulator is always wrong, it is just an approximation in the real world. You should throw a lot of noise on a robot in simulator to be more robust for noise in real world.

![[Pasted image 20220221085213.png]]

![[Pasted image 20220221090408.png]]
Because camera has a static view it always sees the same. So when someone is coming you can check which pixels have changed and erase the rest because they are going to be background.
If you cover the eyes is actually much harder for AI to recognize the face.

![[Pasted image 20220221091102.png]]
If you have a small dataset you will need to create a much more sophisticated pipeline.
In order to debug where is that accuracy loss you should provide perfect data for each step like erasing background with Photoshop, or making alg. memorizing the exact position of the face. Then your Logistic regression should get 100% accuracy. You can check which gave the biggest gain and look for improvment there. Providing perfect data for each component could be done both cumulatively and non-cumulatively.

![[Pasted image 20220221092139.png]]
This can be done too both cumulatively and non-cumulatively.
