## **Lecture 14**
----

In unsupervised training you are given unlabeled data and you are asked to find something interesting about the data.

![[Pasted image 20220221100358.png]]
First alg. is clustering which should figure out these 2 seperate clusters. It is used for market segmentation.

#### k-means alg.
![[Pasted image 20220221101026.png]]
if we want to find 2 clusters:
1. Pick 2 cluster centroids randomly (your best guess).
2. Go for each training example and mark them either as red or blue depending which is closer to which centroid.
3. Compute the average of given class and move there the centroid of that class.
4. GOTO 2 and repeat

After alg. has converged running it should change nothing.

![[Pasted image 20220221101348.png]]
For high dimension spaces you can init centroids by picking k examples from training set and linking it to particular centroid.

![[Pasted image 20220221101518.png]]
in $(a)$ $||x^{(i)}-\mu_j||^2$ is a L2 norm.

![[Pasted image 20220221103503.png]]
This alg. converges basing on given cost function.
According to prof. he usually chooses number of k by hand. Basing on project needs like if you need to segment your market for adv. campaing the marketing department can have bandwith for only 4 paralel campaigns but not 100, that's why choosing k as 4 would be a better decision.
K-means alg. can stuck on local minima. To avoid it you can run it multiple times with different centroids init localization terms and choose the best one.

-----

![[Pasted image 20220221131055.png]]
$p(x) < \epsilon$ means `p of x is very small`
We want to model p(x) to detect cases like that green dot on the chart.
Neither the feature (`Vibration` or `Heat`) is unusuall but the combination of both is.
To create model with such L-shaped data there is no academic or go-to solution. We should rather consider that dataset comes from 2 different Gaussian distributions.

![[Pasted image 20220221131408.png]]
You don't know which examples comes from which Gaussian.
EM alg. allows us to fit the model not knowing which example come from which Gaussian.

![[Pasted image 20220221132416.png]]
`latent` means it exists but you don't get to see value directly.
there is written `are distributed`
Compared to GDA $y^{(i)}$ is changed to latent vector $z$. Also here each Guassian distribution has it own both $\Sigma$ and $\mu$

![[Pasted image 20220221132958.png]]
If we know, but we don't.That's why we can't use this formula.

----

![[Pasted image 20220221133927.png]]
We are also pluging here Bayesian rules. Left term in denominator is from Gaussian and second from multinomial probability.
You compute $w^{(i)}_j$ for every single training example i.
On the bottom there should be $(x^{(i)} - \mu_j)^T$

![[Pasted image 20220221134400.png]]
Sigmoid can be in both direction - it depends on sign. $w^{(i)}_j$ is probability here of each of this examples coming from particular Gaussian distribution.

K-means is assaigning training examples in hard way - like it is linking the example with the closest centroid, no matter what is the difference  in distance between each centroid. EM alg. is more soft in this area - it is assigning the probability of belonging to each centroid for particular training example.

![[Pasted image 20220221135703.png]]
![[Pasted image 20220221135720.png]]
![[Pasted image 20220221135737.png]]
These all shapes are mixture of 2 Gaussians.

![[Pasted image 20220221135856.png]]
EM alg. is exactly trying to max. that product.

![[Pasted image 20220221140046.png]]
All this properties must sum up to 1.

----

That above was just a loose explanation: let's guess the values of $z$, pinch weights to them and plug them into MLE to update them.

`Jensen's inqeuality`
![[Pasted image 20220221141529.png]]
There is written `convex function`

![[Pasted image 20220221141556.png]]
Expected value of x ($Ex$) is average of x which in this examples is 3.
The red point is always higher than green point. Which is another way of seeing why Jensen's inequality holds true.

![[Pasted image 20220221141843.png]]
Straight line is also a convex function.
It says that left hand side is less than right hand side and the only way it is equal is when $x$ is a random variable and always takes on  the same value.

![[Pasted image 20220221142319.png]]
it is from prob. theory but you don't need to bother with that in practical area.

![[Pasted image 20220221142453.png]]
![[Pasted image 20220221142546.png]]
Concave function is a negative of convex function.
We are going to apply the Jensen's inequality to log function.

![[Pasted image 20220221151843.png]]

![[Pasted image 20220221152219.png]]
Keep this picture when going through the math.
What EM alg. does (in E-step) is construct the lower bound that looks like the green one. We want the green curve to be equal to the blue curve at the value of $\Theta$.

![[Pasted image 20220221152418.png]]
M-step takes the green curve and find the maximum. One step of EM will move $\Theta$ to red value.
EM alg. converge only to local optimum.

![[Pasted image 20220221153105.png]]
The last equation on the right is function of the parameters $\Theta$ and is a lower bound for the log-likelihood (the first statement $max ...$)

![[Pasted image 20220221153522.png]]

----

![[Pasted image 20220221154328.png]]
there is written `we want`

![[Pasted image 20220221154152.png]]
$L$ means `is proportional`.
You can choose any distribution for $z^{(i)}$
The only way to make that equation constant is to make sure that denominator and numerator are proportional to each other.

All of this above gives us EM-alg.
![[Pasted image 20220221154530.png]]

We are not trying to maximize the log-likelihood directly, because if you take the mixture of Gaussian model and try to take the derivatives of this and set derivatives equal to 0, there is no known way to solve for the value of $\Theta$ that max. log-likelihood.

## **Lecture 15**
----

![[Pasted image 20220222121507.png]]

![[Pasted image 20220222121927.png]]
One of main differences between this and GDA is that here we got $z^{(i)}$ which is latent vector. It causes a need for such a mechanism which is EM Alg.

![[Pasted image 20220222122137.png]]

![[Pasted image 20220222122553.png]]
We want to maximazie this formula for each parameter $\phi$, $\mu$, $\sigma$.

![[Pasted image 20220222122931.png]]
![[Pasted image 20220222123034.png]]
`...` is just that big formula above.
Here we are deriving the update equations for each parameter.

-----

![[Pasted image 20220222123513.png]]
In Factor analysis model $z^{(i)}$ is now continuos. Sum becomes the integral. Thanks to that, all previously taken steps like Jensen's inequality works the same here.

![[Pasted image 20220222124109.png]]
This procedure is called `coordiante ascent`, because we are maximizing with respect to one coordinate at a time.
You can check if alg. is converging by plotting the loss over iteration and checking if its scoring monotonically and when it plateaus it isn't improving anymore

#### Factor analysis alg.

![[Pasted image 20220222124338.png]]
Possible scenario for mixture of Gaussian - m is much bigger than n.

![[Pasted image 20220222124715.png]]
Scenario where you wouldn't use Gaussian, and Factor analysis can be applied. You have 100 different sensors but data only from 30 last days.

![[Pasted image 20220222124914.png]]
there is written `singular / non-invertible`

![[Pasted image 20220222125356.png]]
Turns out that if your number of training examples is less than the dimension of the data and you plug in the usual formula to derive the MLE of $\sigma$ you end up with the covariance matrix that is singular (the one one the right $[[1,0],[0,0]]$) which means is non-invertible will cause to have $\frac{0}{0}$$ in equation.

![[Pasted image 20220222125645.png]]
The contours will be infinitly skinny.

![[Pasted image 20220222130456.png]]
There is written `diagonal`.
n parameters means there is n diagonal entries.
In this model there is assumption that all of your features are uncorrelated. It allows us to get rid of problem with singular covariance matrix, but it is not very good model for f.e. temp. sensors in the same room.

![[Pasted image 20220222130913.png]]
It means that it has the same entry in every single element on its diagonal. So now we have only 1 parameter (n=1).
Here we assume that every feature is independet and every feature has the same variance as every other feature.

*invers Wishart prior can take the rid of non-invertible covariance matrix, but it is not the best model*
*ML World is not good at scaling it means one alg. will work only with milion examples but for fewer dataset you need completely different alg. And for very small like 100 examples you need another one alg. We, as ML world, don't know the alg that would work for every size of dataset with the same quality of decision boundaries.*

![[Pasted image 20220222133709.png]]
![[Pasted image 20220222133820.png]]
there is written $\Lambda$
We assume that $\Psi$ is diagonal. It is saying that after you compute the mean the noise that you observe at each sensor is independent of the noise at every other sensor.
$d$ is number of forces that are controlling our features. Like what's the number of forces that contoll temp. in a room ?
$\epsilon$ is noise term.

![[Pasted image 20220222135012.png]]
![[Pasted image 20220222135202.png]]
In the begining you only see that red crosses. And with EM alg. you can create model that will find those blue parameters X that describes your dataset.
Here is 2-d data (n=2) but most of the data lies on 1-d subspace (d=1).

![[Pasted image 20220222135831.png]]
With $\Lambda z + \mu$ we can map from 2d to 3d data.
Then for each point in that 3-d space we have a little Gaussian Bump and add a little noise to them. We end up with similar red crosses but in 3-D space.
Factor Analysis alg. is able to capture the model for dataset that lyies on the common surface like 2d pancake in 3d dimensions with a little fuzzy noise.
FA alg can take very high dimensional data (100 dim. data) and model the data as roughly lying on 3-d/5d subspace with a little bit of fuzz of that low dim. subspace.

![[Pasted image 20220222140457.png]]
*even in 2d you have freedom to choose Gaussian noises like that - red crosses can be drawn here pretty far from subspace*
*with a very high dimensional set and low examples when data doesn't lie in a subspace this model may not be the best model, but on the other hand, with such dataset you can't fit very complex models anyway, so this one may be pretty reasonable.*

![[Pasted image 20220222141500.png]]
x is `partition vector`

![[Pasted image 20220222141744.png]]
Here we can see the calculation of marginal distribution

![[Pasted image 20220222142047.png]]
and here the conditional of Gaussian distribution.

![[Pasted image 20220222150006.png]]
![[Pasted image 20220222150045.png]]
All sigmas are derived similarly.

![[Pasted image 20220222150618.png]]
$\Psi$ is covariance of epsilon.
In $E[zz^T]$ because $z$ is drawn from std. Gaussian with identity covariance that expectation is just the identity.

![[Pasted image 20220222150745.png]]
Going for the same process with rest of blocks gives you this.

![[Pasted image 20220222150950.png]]
$P(x^{(i)})$ will be this Gaussian density. You could take derivatives of the log likelihood with the respect to the parameters and set param. to 0 and solve to find out that there is no known closed-form solution. That's why we are going to resort to EM.

![[Pasted image 20220222151740.png]]
You compute these equation on the right to represent $Q_i$

![[Pasted image 20220222151526.png]]
these equations map to each other.

![[Pasted image 20220222152312.png]]
*Often big integral can be represented with expected value*

![[Pasted image 20220222152800.png]]
*thanks to that we got $log$ before gaussian density function, it can eliminate $exp$ in gaussian density equation and it comes out that we are maximizing quadratic function*